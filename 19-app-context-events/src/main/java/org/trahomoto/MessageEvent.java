package org.trahomoto;

import org.springframework.context.ApplicationEvent;

/**
 * Created by Vladimir on 27.03.2016.
 */
public class MessageEvent extends ApplicationEvent {

    private String message;

    public MessageEvent(Object source, String message) {
        super(source);
        this.message = message;
    }

     public String getMessage(){
        return message;
    }
}
