package org.trahomoto;

import org.springframework.context.ApplicationListener;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Vladimir on 27.03.2016.
 */
public class Main {

    public static void main(String[] args) {

        try(ClassPathXmlApplicationContext ctx
                    = new ClassPathXmlApplicationContext("classpath:app-context-xml.xml")){
            Publisher myPublisher = ctx.getBean("myPublisher", Publisher.class);

//            ctx.addApplicationListener(ctx.getBean("listenerFoo", ApplicationListener.class));
//            ctx.addApplicationListener(ctx.getBean("listenerBar", ApplicationListener.class));

            System.out.println("~~~~~~~~~~~~~~~~~~~~~");
            myPublisher.publish("Hello world!");


            System.out.println("~~~~~~~~~~~~~~~~~~~~~");
            //ctx.addApplicationListener(ctx.getBean("listenerLazy", ApplicationListener.class));
            ctx.getBean("listenerLazy");
            myPublisher.publish("Ololo foo bar!");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~");

        }

    }

}
