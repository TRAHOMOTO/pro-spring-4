package org.trahomoto;

import org.springframework.context.ApplicationListener;

/**
 * Created by Vladimir on 27.03.2016.
 */
public class MessageEventListener implements ApplicationListener<MessageEvent> {
    @Override
    public void onApplicationEvent(MessageEvent event) {
        System.out.println(name+" === Received: " +event.getMessage());
    }

    private String name;

    public MessageEventListener(String name){
        this.name = name;
    }

}
