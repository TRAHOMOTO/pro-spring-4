package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.util.StopWatch;

public class Main {
    public static void main(String[] args) {

        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();


        DemoBean absBean = ctx.getBean("abstractLookupBean", DemoBean.class);
        DemoBean stdBean = ctx.getBean("standardLookupBean", DemoBean.class);

        System.out.println("Abstract method injection ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        displayinfo(absBean);
        System.out.println("Standard setter injection ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        displayinfo(stdBean);


    }


    public static void displayinfo(DemoBean bean) {

        MyHelper helperl = bean.getHelper();
        MyHelper helper2 = bean.getHelper();

        System.out.println("Helper Instances the Same?: " + (helperl == helper2));
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("lookupDemo");

        for (int х = 0; х < 100000; х++) {
            MyHelper helper = bean.getHelper();
            helper.doHardWork();
        }

        stopWatch.stop();
        System.out.println("100000 gets took " + stopWatch.getTotalTimeMillis() + " ms");
    }

}
