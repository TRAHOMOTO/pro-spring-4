package org.trahomoto;

public interface DemoBean {
    MyHelper getHelper();
    void someOperation();
}
