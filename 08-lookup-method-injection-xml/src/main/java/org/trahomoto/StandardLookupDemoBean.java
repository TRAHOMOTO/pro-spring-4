package org.trahomoto;

public class StandardLookupDemoBean implements DemoBean {

    private MyHelper helper;

    public void setHelper(MyHelper helper) {
        this.helper = helper;
    }

    @Override
    public MyHelper getHelper() {
        return helper;
    }

    @Override
    public void someOperation() {
        helper.doHardWork();
    }
}
