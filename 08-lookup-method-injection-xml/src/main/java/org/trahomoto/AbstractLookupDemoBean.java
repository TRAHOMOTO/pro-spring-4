package org.trahomoto;

public abstract class AbstractLookupDemoBean implements DemoBean {
    @Override
    public abstract MyHelper getHelper();

    @Override
    public void someOperation() {
        getHelper().doHardWork();
    }
}
