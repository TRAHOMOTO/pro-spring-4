package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {

    public static void main(String[] args) {

        GenericXmlApplicationContext ctx
                = new GenericXmlApplicationContext("classpath:app-context-xml.xml");

        MessageRenderer renderer = ctx.getBean("messageRenderer", MessageRenderer.class);
        renderer.render();

    }

}
