package org.trahomoto.impl;

import org.trahomoto.MessageProvider;

import javax.inject.Inject;
import javax.inject.Named;

@Named("messageProvider")
public class ConfigurableMsgProvider implements MessageProvider {

    private String message = "Default message";

    public ConfigurableMsgProvider() {}

    @Inject
    @Named("message")
    public ConfigurableMsgProvider(String message) {
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
