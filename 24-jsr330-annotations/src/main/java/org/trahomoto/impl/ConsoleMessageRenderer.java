package org.trahomoto.impl;

import org.trahomoto.MessageProvider;
import org.trahomoto.MessageRenderer;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Named("messageRenderer")
public class ConsoleMessageRenderer implements MessageRenderer {
    @Inject
    @Named("messageProvider")
    private MessageProvider provider;

    @Override
    public void render() {
        System.out.println(
                getMessageProvider().getMessage()
        );
    }


    @Override
    public void setMessageProvider(MessageProvider provider) {
        this.provider = provider;
    }

    @Override
    public MessageProvider getMessageProvider() {
        return provider;
    }
}
