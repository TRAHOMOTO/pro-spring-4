package org.trahomoto;

public class RastaOracle implements Oracle {
    @Override
    public String tellReasonOfLife() {
        return "Джебать ждеб с бро!";
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RastaOracle{");
        sb.append(tellReasonOfLife());
        sb.append('}');
        return sb.toString();
    }
}
