package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class CollectionInjection {


    private List listField;
    private Map<String, Object> mapField;
    private Set setField;
    private Properties propertiesField;


    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();

        CollectionInjection targetBean = ctx.getBean("targetInjectionBean", CollectionInjection.class);

        System.out.println("\nList ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        targetBean.getListField().forEach(System.out::println);
        System.out.println("\nMap ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        targetBean.getMapField().forEach((k,v)-> System.out.println(k+": "+v));
        System.out.println("\nProp ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        targetBean.getPropertiesField().forEach((k,v)-> System.out.println(k+": "+v));
        System.out.println("\nSet ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        targetBean.getSetField().forEach(System.out::println);

    }


    public List getListField() {
        return listField;
    }

    public void setListField(List listField) {
        this.listField = listField;
    }

    public Map<String, Object> getMapField() {
        return mapField;
    }

    public void setMapField(Map<String, Object> mapField) {
        this.mapField = mapField;
    }

    public Set getSetField() {
        return setField;
    }

    public void setSetField(Set setField) {
        this.setField = setField;
    }

    public Properties getPropertiesField() {
        return propertiesField;
    }

    public void setPropertiesField(Properties propertiesField) {
        this.propertiesField = propertiesField;
    }
}
