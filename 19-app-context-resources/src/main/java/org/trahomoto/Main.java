package org.trahomoto;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;

/**
 * Created by Vladimir on 27.03.2016.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext();

        File file = File.createTempFile("test", "txt");
        file.deleteOnExit();

        System.out.println(file.getPath());
//        Resource resl = ctx.getResource("file://" + file.getPath());
//        displayinfo(resl);
        Resource res2 = ctx.getResource("classpath:test.txt");
        displayinfo(res2);
        Resource resЗ = ctx.getResource("http://www.google.ru");
        displayinfo(resЗ);
    }
    private static void displayinfo(Resource res) throws Exception {
        System.out.println(res.getClass());
        System.out.println(res.getURL().getContent());
        System.out.println("");
    }

}
