package org.trahomoto.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.trahomoto.MessageProvider;
import org.trahomoto.MessageRenderer;
import org.trahomoto.impl.ConfigurableMsgProvider;
import org.trahomoto.impl.ConsoleMessageRenderer;


@Configuration
public class AppConfig {

    @Bean
    public MessageProvider messageProvider(){
        return new ConfigurableMsgProvider();
    }

    @Bean
    public MessageRenderer messageRenderer(){
        MessageRenderer mr = new ConsoleMessageRenderer();
        mr.setMessageProvider(messageProvider());

        return mr;
    }

}
