package org.trahomoto;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Created by Vladimir on 06.04.2016.
 */
public class MyAwareBeanImpl
        implements BeanNameAware, BeanFactoryAware, ApplicationContextAware {

    private String name;
    private BeanFactory beanFactory;
    private ApplicationContext applicationContext;


    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    @Override
    public void setBeanName(String name) {
        this.name = name;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MyAwareBeanImpl{");
        sb.append("name='").append(name).append('\'');
        sb.append(", beanFactory=").append(beanFactory);
        sb.append(", applicationContext=").append(applicationContext);
        sb.append('}');
        return sb.toString();
    }
}
