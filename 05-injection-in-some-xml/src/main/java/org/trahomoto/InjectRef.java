package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.net.URISyntaxException;

public class InjectRef {

    private Oracle oracle;

    public Oracle getOracle() {
        return oracle;
    }

    public void setOracle(Oracle oracle) {
        this.oracle = oracle;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InjectRef{");
        sb.append("oracle=").append(oracle);
        sb.append('}');
        return sb.toString();
    }

    public static void main(String[] args) throws URISyntaxException {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();

        InjectRef injectRef = ctx.getBean("ololoFooBar", InjectRef.class);
        System.out.println(injectRef.getOracle().tellReasonOfLife());
        injectRef = ctx.getBean("fooBaz", InjectRef.class);
        System.out.println(injectRef.getOracle().tellReasonOfLife());
        injectRef = ctx.getBean("coolBar", InjectRef.class);
        System.out.println(injectRef.getOracle().tellReasonOfLife());
    }

}
