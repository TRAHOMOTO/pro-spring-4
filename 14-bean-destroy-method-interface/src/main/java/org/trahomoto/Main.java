package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * Created by komunar on 21.03.2016.
 */
public class Main {

    public static final boolean USE_AUTO_DESTROY = true;

    public static void main(String[] args) {

        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext("classpath:app-context-xml.xml");


        MyCoolBean bean1 = ctx.getBean("bean1", MyCoolBean.class);
        MyCoolBean bean2 = ctx.getBean("bean2", MyCoolBean.class);
        MyCoolBean bean3 = ctx.getBean("bean3", MyCoolBean.class);

        if (USE_AUTO_DESTROY) {
            ctx.registerShutdownHook();
        } else {
            System.out.println("Calling destroy()"); //  вызов метода  destroy()
            ctx.destroy();
            System.out.println("Called  destroy()");  // метод destroy()  вызван
        }
    }
}
