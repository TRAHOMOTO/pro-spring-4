package org.trahomoto;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Vladimir on 27.03.2016.
 */
public class Main {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx
                = new ClassPathXmlApplicationContext("classpath:app-context-xml.xml");

        System.out.println("=======================================================================");
        ctx.getBean("svetik");
        ctx.getBean("vovan");
        System.out.println("=======================================================================");
    }

}
