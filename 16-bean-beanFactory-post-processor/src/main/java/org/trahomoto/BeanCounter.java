package org.trahomoto;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import java.util.logging.Logger;

/**
 * Created by Vladimir on 02.04.2016.
 */
public class BeanCounter implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory)
            throws BeansException {
        System.out.println("Кол-во бинов: "+ factory.getBeanDefinitionCount());
    }
}
