package org.trahomoto;

/**
 * Created by Vladimir on 02.04.2016.
 */
public class MyCoolBean {

    private String name;
    private int age;

    public MyCoolBean(){}


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MyCoolBean{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }





}
