package org.trahomoto;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by komunar on 21.03.2016.
 */
public class MyCoolBean {

    public static final String DEFAULT_NAME = "Кондратьев Вова";

    private String name = "";
    private int age = Integer.MIN_VALUE;

    public MyCoolBean() {
        System.out.println("Вызов конструктора");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("Вызов удалятора "+this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MyCoolBean{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }
}
