package org.trahomoto;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * Created by komunar on 21.03.2016.
 */
public class Main {

    public static void main(String[] args) {

        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext("classpath:app-context-xml.xml");

        MessageDigester digester = ctx.getBean("coolBean", MessageDigester.class);

        digester.digest("Hello World!");
    }
}
