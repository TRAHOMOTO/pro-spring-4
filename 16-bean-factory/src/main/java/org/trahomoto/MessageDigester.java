package org.trahomoto;

import java.security.MessageDigest;

/**
 * Created by komunar on 21.03.2016.
 */
public class MessageDigester {
    private MessageDigest digest1;
    private MessageDigest digest2;

    public void digest(String message){
        System.out.println("Using  digestl");
        digest(message,  getDigest1());
        System.out.println("Using  digest2");
        digest(message,  getDigest2());
    }


    private void digest(String msg, MessageDigest digest) {
        System.out.println("Using  alogrithm:  " + digest.getAlgorithm());
        digest.reset();
        byte[] bytes = msg.getBytes();
        byte[] out = digest.digest(bytes);
        System.out.println(out);
    }


    public MessageDigest getDigest1() {
        return digest1;
    }

    public void setDigest1(MessageDigest digest1) {
        this.digest1 = digest1;
    }

    public MessageDigest getDigest2() {
        return digest2;
    }

    public void setDigest2(MessageDigest digest2) {
        this.digest2 = digest2;
    }
}
