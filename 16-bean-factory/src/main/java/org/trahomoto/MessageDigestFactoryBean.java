package org.trahomoto;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import java.security.MessageDigest;


public class MessageDigestFactoryBean implements FactoryBean<MessageDigest>, InitializingBean {

    String algorithmName = "MD5";
    private MessageDigest digest;

    @Override
    public MessageDigest getObject() throws Exception {
        return digest;
    }

    @Override
    public Class<MessageDigest> getObjectType() {
        return MessageDigest.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        digest = MessageDigest.getInstance(getAlgorithmName());
    }

    private String getAlgorithmName() {
        return algorithmName;
    }

    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }
}
