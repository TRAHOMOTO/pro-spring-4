package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.Locale;

/**
 * Created by Vladimir on 27.03.2016.
 */
public class Main {

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx
                = new GenericXmlApplicationContext();

        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();

        Locale english = Locale.ENGLISH;
        Locale ukrainian = new Locale("ua", "UK");
        Locale sysDefault = Locale.getDefault();

        System.out.println(ctx.getMessage("nameMsg", null, english));
        System.out.println(ctx.getMessage("nameMsg", null, ukrainian));
        System.out.println(ctx.getMessage("nameMsg", null, sysDefault));
        System.out.println();
        System.out.println(ctx.getMessage("nameMsg", new Object[]{"Chris", "Schaefer"}, english));
        System.out.println(ctx.getMessage("nameMsg", new Object[]{"Вован", "Красава"}, sysDefault));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(ctx.getMessage("msg", null, english));
        System.out.println(ctx.getMessage("msg", null, ukrainian));
        System.out.println(ctx.getMessage("msg", null, sysDefault));
    }

}
