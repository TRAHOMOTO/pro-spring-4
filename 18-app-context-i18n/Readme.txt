Как организовать интернационализацию в приложении.

Чтобы задействовать поддржку MessageSource, предоставляемую Application
Context, в конфигурации должен быть определен бин типа MessageSource с име­
нем messageSource. Контекст ApplicationContext берет этот MessageSource и
вкладывает его внутрь себя самого, разрешая доступ к сообщениям с применением
ApplicationContext.
