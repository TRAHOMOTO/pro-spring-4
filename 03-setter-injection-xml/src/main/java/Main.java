import org.springframework.context.support.GenericXmlApplicationContext;
import org.trahomoto.MessageRenderer;

import java.net.URISyntaxException;

public class Main {

    public static void main(String[] args) throws URISyntaxException {

        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();

        MessageRenderer renderer = ctx.getBean("messageRenderer", MessageRenderer.class);

        System.out.println("\n------------------------------------------------\n");

        renderer.render();
    }

}
