import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.trahomoto.MessageRenderer;

import java.net.URISyntaxException;

public class Main {

    public static void main(String[] args) throws URISyntaxException {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("app-context.xml");


        MessageRenderer renderer = ctx.getBean("renderer", MessageRenderer.class);

        System.out.println("\n------------------------------------------------\n");
        renderer.render();
    }

}
