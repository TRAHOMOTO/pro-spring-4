package org.trahomoto;

import bro.ba.lg.utils.BaggerCoolLib;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.util.StopWatch;

public class Main {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();

        BaggerCoolLib bean1 = ctx.getBean("replaceTarget", BaggerCoolLib.class);
        BaggerCoolLib bean2 = ctx.getBean("standartBean", BaggerCoolLib.class);

        displayinfo(bean1);
        displayinfo(bean2);
    }

    private static void displayinfo(BaggerCoolLib target) {
        System.out.println(target.formatMessage("Hello World!"));

        StopWatch stopWatch = new StopWatch();
        stopWatch.start("perfTest");

        for (int х = 0; х < 1000000; х++) {
            String out = target.formatMessage("foo");
        }

        stopWatch.stop();
        System.out.println("l000000 invocations took: " + stopWatch.getTotalTimeMillis() + " ms");
    }
}
