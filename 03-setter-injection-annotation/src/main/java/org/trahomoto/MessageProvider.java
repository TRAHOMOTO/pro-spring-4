package org.trahomoto;

public interface MessageProvider {
    String getMessage();
}
