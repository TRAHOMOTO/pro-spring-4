package org.trahomoto.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trahomoto.MessageProvider;
import org.trahomoto.MessageRenderer;

@Service("messageRenderer")
public class ConsoleMessageRenderer implements MessageRenderer {

    private MessageProvider provider;

    @Override
    public void render() {
        System.out.println(
                getMessageProvider().getMessage()
        );
    }
    @Autowired
    @Override public void setMessageProvider(MessageProvider provider) {
        this.provider = provider;
    }

    @Override
    public MessageProvider getMessageProvider() {
        return provider;
    }
}
