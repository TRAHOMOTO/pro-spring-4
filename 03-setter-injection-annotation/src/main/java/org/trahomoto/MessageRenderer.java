package org.trahomoto;

public interface MessageRenderer {
    void render();
    void setMessageProvider(MessageProvider renderer);
    MessageProvider getMessageProvider();
}
