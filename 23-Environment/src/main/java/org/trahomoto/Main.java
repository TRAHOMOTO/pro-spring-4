package org.trahomoto;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;

import java.util.HashMap;
import java.util.Map;

/**
 * Пример получения значений параметров окружения из интерфейса Environment
 */
public class Main {

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.refresh();

        ConfigurableEnvironment env = ctx.getEnvironment();
        MutablePropertySources propertySources = env.getPropertySources();  // стандартная реализация интерфейса
                                                                            // PropertySources, которая позволяет манипулировать
                                                                            // содержащимися источниками свойств

        Map аррМар = new HashMap();
        аррМар.put("application.home", "bazBar");
        аррМар.put("user.home", "fooBar"); // переопределяет свойство

        propertySources.addLast (new MapPropertySource("PROSPRING4_МАР" , аррМар)) ;

        /*
            При использовании интерфейса Environment видно, что
            определенное нами свойство user.home получает преимущество, поскольку оно оп­
            ределено как первое для поиска значений свойств.

            propertySources.addFirst (new MapPropertySource("PROSPRING4_МАР" , аррМар));
        */

        // Получение значений из System
        System.out.println("user.home: "+ System.getProperty("user.home"));
        System.out.println("JAVA_HOМE "+ System.getenv("JAVA_HOME"));
        // Получение тех же значений из Environment
        System.out.println("user.home: "+ env.getProperty("user.home"));
        System.out.println( "JAVA_HOМE: "+ env.getProperty("JAVA_HOME"));
        // Получение кастомных свойств
        System.out.println("application.home: " + env.getProperty("application.home"));
    }

}
