package org.trahomoto.members;

import org.trahomoto.instruments.Instrument;

public class Instrumentalist implements Performer {

    public Instrumentalist() {}

    @Override
    public void perform() throws Exception {
        System.out.print("Playing: " + song + " : ");
        instrument.play();
    }

    private String song;
    private Instrument instrument;

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public String screamSong() {
        return song;
    }

    public void setInstrument(Instrument instrument) {  // Внедрение
        this.instrument = instrument;                   // инструмента
    }

}
