package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();

        Human h1 = ctx.getBean("bagger", Human.class);
        Human h2 = ctx.getBean("recar", Human.class);

        System.out.println(h1);
        System.out.println(h2);
    }
}
