package org.trahomoto;

public interface Oracle {
    String tellReasonOfLife();
}
