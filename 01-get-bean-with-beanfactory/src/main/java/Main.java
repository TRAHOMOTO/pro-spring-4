import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.ClassPathResource;
import org.trahomoto.Oracle;

import java.net.URISyntaxException;

public class Main {

    public static void main(String[] args) throws URISyntaxException {
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();

        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory);
        reader.loadBeanDefinitions(new ClassPathResource("app-context-factory.xml"));

        Oracle vovan = (Oracle) factory.getBean("oracle");
        //Oracle vovan = (Oracle) factory.getBean("klesh");
        //Oracle vovan = (Oracle) factory.getBean("rastaMan");

        System.out.println(vovan.tellReasonOfLife());
    }

}
