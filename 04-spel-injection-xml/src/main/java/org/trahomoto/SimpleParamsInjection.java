package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;

import java.net.URISyntaxException;

public class SimpleParamsInjection {

    private String name;
    private int age;
    private boolean isProgrammist;
    private float heigh;
    private Long ageInSecs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isProgrammist() {
        return isProgrammist;
    }

    public void setProgrammist(boolean programmist) {
        isProgrammist = programmist;
    }

    public float getHeigh() {
        return heigh;
    }

    public void setHeigh(float heigh) {
        this.heigh = heigh;
    }

    public Long getAgeInSecs() {
        return ageInSecs;
    }

    public void setAgeInSecs(Long ageInSecs) {
        this.ageInSecs = ageInSecs;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("org.trahomoto.SimpleParamsInjection{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append(", isProgrammist=").append(isProgrammist);
        sb.append(", heigh=").append(heigh);
        sb.append(", ageInSecs=").append(ageInSecs);
        sb.append('}');
        return sb.toString();
    }

    public static void main(String[] args) throws URISyntaxException {

        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();

        SimpleParamsInjection vovan = ctx.getBean("simpleExample", SimpleParamsInjection.class);

        System.out.println(vovan);
        
    }

}
