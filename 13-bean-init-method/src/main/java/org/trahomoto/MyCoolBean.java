package org.trahomoto;

/**
 * Created by komunar on 21.03.2016.
 */
public class MyCoolBean {

    public static final String DEFAULT_NAME = "Кондратьев Вова";

    private String name = "";
    private int age = Integer.MIN_VALUE;

    public MyCoolBean() {
        System.out.println("Вызов конструктора");
    }

    public void init(){
        System.out.println("Вызов инициализатора");

        if(name.isEmpty()){
            System.out.println("Присвоено дефолтовое значение name: "+DEFAULT_NAME);
            name = DEFAULT_NAME;
        }

        if (age == Integer.MIN_VALUE){
            throw new IllegalArgumentException("Поле age имеет недопустимое значение "+Integer.MIN_VALUE);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
