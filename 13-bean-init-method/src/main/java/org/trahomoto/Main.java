package org.trahomoto;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by komunar on 21.03.2016.
 */
public class Main {
    public static void main(String[] args) {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("app-context-xml.xml");

        MyCoolBean bean1 = ctx.getBean("bean1", MyCoolBean.class);
        MyCoolBean bean2 = ctx.getBean("bean2", MyCoolBean.class);
        MyCoolBean bean3 = ctx.getBean("bean3", MyCoolBean.class);


    }
}
