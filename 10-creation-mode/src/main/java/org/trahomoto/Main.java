package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();

        System.out.println("Singleton ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        MyCoolBean bean1_1 = ctx.getBean("single1", MyCoolBean.class);
        MyCoolBean bean2_1 = ctx.getBean("single2", MyCoolBean.class);
        MyCoolBean bean3_1 = ctx.getBean("single3", MyCoolBean.class);

        MyCoolBean bean1_2 = ctx.getBean("single1", MyCoolBean.class);
        MyCoolBean bean2_2 = ctx.getBean("single2", MyCoolBean.class);
        MyCoolBean bean3_2 = ctx.getBean("single3", MyCoolBean.class);

        System.out.println("bean1_1 == bean1_2: " + (bean1_1 == bean1_2));
        System.out.println("bean2_1 == bean2_2: " + (bean2_1 == bean2_2));
        System.out.println("bean3_1 == bean3_2: " + (bean3_1 == bean3_2));

        System.out.println("Multitone ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        MyCoolBean mbean1_1 = ctx.getBean("multi1", MyCoolBean.class);
        MyCoolBean mbean2_1 = ctx.getBean("multi2", MyCoolBean.class);
        MyCoolBean mbean3_1 = ctx.getBean("multi3", MyCoolBean.class);

        MyCoolBean mbean1_2 = ctx.getBean("multi1", MyCoolBean.class);
        MyCoolBean mbean2_2 = ctx.getBean("multi2", MyCoolBean.class);
        MyCoolBean mbean3_2 = ctx.getBean("multi3", MyCoolBean.class);

        System.out.println("mbean1_1 == mbean1_2: " + (mbean1_1 == mbean1_2));
        System.out.println("mbean2_1 == mbean2_2: " + (mbean2_1 == mbean2_2));
        System.out.println("mbean3_1 == mbean3_2: " + (mbean3_1 == mbean3_2));
    }
}
