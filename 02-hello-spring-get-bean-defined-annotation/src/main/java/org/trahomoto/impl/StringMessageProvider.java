package org.trahomoto.impl;

import org.springframework.stereotype.Service;
import org.trahomoto.MessageProvider;

@Service("messageProvider")
public class StringMessageProvider implements MessageProvider {
    @Override
    public String getMessage() {
        return "Fooo bar ololo! Spring say HELLLO!";
    }
}
