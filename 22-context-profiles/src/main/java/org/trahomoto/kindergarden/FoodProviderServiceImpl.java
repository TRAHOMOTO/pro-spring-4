package org.trahomoto.kindergarden;

import org.trahomoto.Food;
import org.trahomoto.FoodProviderService;

import java.util.ArrayList;
import java.util.List;

/**
 * Обеденный набор дошколяты
 */
public class FoodProviderServiceImpl implements FoodProviderService {
    @Override
    public List<Food> provideLunchSet() {
        List<Food> foods = new ArrayList<>();

        foods.add(new Food("Молоко"));
        foods.add(new Food("Булочка"));

        return foods;
    }
}
