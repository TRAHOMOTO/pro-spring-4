package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * Для активации профиля нужно запустить JVM c аргументами
 *
 * -Dspring.profiles.active="kindergarden"
 * -Dspring.profiles.active="highscool"
 */
public class Main {

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();

        ctx.load("classpath:*-config.xml"); // Загрузка ВСЕХ файлов конфигураций
        ctx.refresh();

        // Получение ссылки на сервис
        FoodProviderService foodService = ctx.getBean("foodsProvider", FoodProviderService.class);
        foodService.provideLunchSet().forEach(food -> System.out.println(food.getName()));

    }
}
