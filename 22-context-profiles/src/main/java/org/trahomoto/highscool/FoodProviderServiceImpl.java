package org.trahomoto.highscool;

import org.trahomoto.Food;
import org.trahomoto.FoodProviderService;

import java.util.ArrayList;
import java.util.List;

/**
 * Обеденный набор старшекласника
 */
public class FoodProviderServiceImpl implements FoodProviderService {
    @Override
    public List<Food> provideLunchSet() {
        List<Food> foods = new ArrayList<>();

        foods.add(new Food("Revo"));
        foods.add(new Food("Чебурек"));
        foods.add(new Food("Кофе"));

        return foods;
    }
}
