package org.trahomoto;

import java.util.List;

/**
 * Created by Vladimir on 17.04.2016.
 */
public interface FoodProviderService {
    List<Food> provideLunchSet();
}
