package org.trahomoto;

import java.beans.PropertyEditorSupport;

/**
 * Created by Vladimir on 27.03.2016.
 */
public class NamePropertyEditor extends PropertyEditorSupport {
    @Override
    public void setAsText(String text) throws IllegalArgumentException {

        String[] parts = text.split("\\s");

        setValue( new Name(parts[0], parts[1]));
    }
}
