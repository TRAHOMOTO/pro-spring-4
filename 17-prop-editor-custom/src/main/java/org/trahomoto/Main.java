package org.trahomoto;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Vladimir on 27.03.2016.
 */
public class Main {

    private Name name;

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:app-context-xml.xml");

        System.out.println("~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(ctx.getBean("exampleBean", Main.class).getName());
        System.out.println("~~~~~~~~~~~~~~~~~~~~~");
    }
}
