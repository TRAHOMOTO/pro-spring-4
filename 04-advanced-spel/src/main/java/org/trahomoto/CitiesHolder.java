package org.trahomoto;

import java.util.List;

/**
 * Бин в который внедряется коллекция
 */
public class CitiesHolder {

    private List<City> cities;
    private List<String> cityTitles;

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public List<String> getCityTitles() {
        return cityTitles;
    }

    public void setCityTitles(List<String> cityTitles) {
        this.cityTitles = cityTitles;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CitiesHolder{");
        sb.append("cities=").append(cities);
        sb.append(", cityTitles=").append(cityTitles);
        sb.append('}');
        return sb.toString();
    }
}
