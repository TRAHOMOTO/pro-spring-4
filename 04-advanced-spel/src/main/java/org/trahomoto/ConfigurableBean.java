package org.trahomoto;

/**
 * Бин, который как то конфигурируется
 */
public class ConfigurableBean {

    private String foo;
    private String ololo;
    private String homeDir;


    public String getFoo() {
        return foo;
    }

    public void setFoo(String foo) {
        this.foo = foo;
    }

    public String getOlolo() {
        return ololo;
    }

    public void setOlolo(String ololo) {
        this.ololo = ololo;
    }

    public String getHomeDir() {
        return homeDir;
    }

    public void setHomeDir(String homeDir) {
        this.homeDir = homeDir;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ConfigurableBean{");
        sb.append("foo='").append(foo).append('\'');
        sb.append(", ololo='").append(ololo).append('\'');
        sb.append(", homeDir='").append(homeDir).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
