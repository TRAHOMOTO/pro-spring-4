package org.trahomoto;

/**
 * Created by Vladimir on 28.03.2016.
 */
public class CityHolder {
    private City city;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
