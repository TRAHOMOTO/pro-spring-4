package org.trahomoto;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Vladimir on 28.03.2016.
 */
public class Main {
    public static void main(String[] args) {

        ClassPathXmlApplicationContext ctx
                = new ClassPathXmlApplicationContext("classpath:app-context-xml.xml");

        // Внедрение случайного города из коллекции
        System.out.println("Внедрение случайного города из коллекции ~~~~~~~~~~~~~~");
        CityHolder holder1 = ctx.getBean("rndCity", CityHolder.class);
        System.out.println(holder1.getCity());
        System.out.println();

        // Внедрение города из Map
        System.out.println("Внедрение города из Map ~~~~~~~~~~~~~~");
        CityHolder holder2 = ctx.getBean("mapCity", CityHolder.class);
        System.out.println(holder2.getCity());
        System.out.println();

        // Внедрение конфига из Properties и из environment
        System.out.println("Внедрение конфига из Properties ~~~~~~~~~~~~~~");
        ConfigurableBean configured = ctx.getBean("needConfig", ConfigurableBean.class);
        System.out.println(configured);
        System.out.println();

        // Внедрение отфильтрованного списка городов
        System.out.println("Внедрение отфильтрованного списка городов ~~~~~~~~~~~~~~");
        CitiesHolder bigCities = ctx.getBean("bigCities", CitiesHolder.class);
        System.out.println(bigCities.getCities());
        System.out.println(bigCities.getCityTitles());
        System.out.println();

        // Внедрение первого крупного города
        System.out.println("Внедрение первого крупного города ~~~~~~~~~~~~~~");
        CityHolder holder3 = ctx.getBean("aBigCity", CityHolder.class);
        System.out.println(holder3.getCity());
        System.out.println();


        // Внедрение первого крупного города
        System.out.println("Внедрение отфильтрованного списка городов ~~~~~~~~~~~~~~");
        CitiesHolder bigCitiesWTitles = ctx.getBean("bigCitiesWithTitles", CitiesHolder.class);
        System.out.println(bigCitiesWTitles.getCities());
        System.out.println(bigCitiesWTitles.getCityTitles());
        System.out.println();


    }
}
