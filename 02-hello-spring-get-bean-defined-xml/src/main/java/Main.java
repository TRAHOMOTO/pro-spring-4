import org.springframework.context.support.GenericXmlApplicationContext;
import org.trahomoto.MessageProvider;
import org.trahomoto.MessageRenderer;

import java.net.URISyntaxException;

public class Main {

    public static void main(String[] args) throws URISyntaxException {

        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();

        MessageProvider provider = ctx.getBean("messageProvider", MessageProvider.class);
        MessageRenderer renderer = ctx.getBean("messageRenderer", MessageRenderer.class);

        System.out.println("\n------------------------------------------------\n");
        renderer.setMessageProvider(provider);
        renderer.render();
    }

}
