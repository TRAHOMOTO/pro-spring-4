package org.trahomoto;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.trahomoto.members.Performer;


public class Main {

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:app-context-xml.xml");

        System.out.println("~~~~~~~~~~~~~~~~~~~~~");
        ctx.getBean("kenny", Performer.class).perform();
        System.out.println("~~~~~~~~~~~~~~~~~~~~~");
    }

}
