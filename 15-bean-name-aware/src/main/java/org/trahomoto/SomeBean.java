package org.trahomoto;

import org.springframework.beans.factory.BeanNameAware;

/**
 * Created by komunar on 21.03.2016.
 */
public class SomeBean implements BeanNameAware {
    @Override
    public void setBeanName(String s) {
        System.out.println("Имя бина: "+s);
    }
}
