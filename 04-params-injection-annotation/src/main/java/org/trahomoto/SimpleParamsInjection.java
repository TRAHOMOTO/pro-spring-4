package org.trahomoto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;

@Service("sampleInjectionParams")
public class SimpleParamsInjection {
    @Value("Vovan")
    private String name;
    @Value("31")
    private int age;
    @Value("true")
    private boolean isProgrammist;
    @Value("1.85")
    private float heigh;
    @Value("12312312313")
    private Long ageInSecs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isProgrammist() {
        return isProgrammist;
    }

    public void setProgrammist(boolean programmist) {
        isProgrammist = programmist;
    }

    public float getHeigh() {
        return heigh;
    }

    public void setHeigh(float heigh) {
        this.heigh = heigh;
    }

    public Long getAgeInSecs() {
        return ageInSecs;
    }

    public void setAgeInSecs(Long ageInSecs) {
        this.ageInSecs = ageInSecs;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("org.trahomoto.SimpleParamsInjection{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append(", isProgrammist=").append(isProgrammist);
        sb.append(", heigh=").append(heigh);
        sb.append(", ageInSecs=").append(ageInSecs);
        sb.append('}');
        return sb.toString();
    }

    public static void main(String[] args) throws URISyntaxException {

        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-annotation.xml");
        ctx.refresh();

        SimpleParamsInjection vovan = ctx.getBean("sampleInjectionParams", SimpleParamsInjection.class);

        System.out.println(vovan);
        
    }

}
