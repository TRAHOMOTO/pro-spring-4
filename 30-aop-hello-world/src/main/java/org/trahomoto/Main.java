package org.trahomoto;

import org.springframework.aop.framework.ProxyFactory;

/**
 * Мы возьмем простой класс, выводящий сообщение "World", и с при­
 * менением АОП трансформируем экземпляр этого класса во время выполнения, что­
 * бы он выводил сообщение "Hello World!".
 */
public class Main {

    public static void main(String[] args) {

        MessageWriter target = new MessageWriter();

        ProxyFactory pf = new ProxyFactory();
        pf.addAdvice(new MessageDecorator());
        pf.setTarget(target);

        MessageWriter targetProxy = (MessageWriter) pf.getProxy();

        System.out.println("\nTarget ~~~~~~~~~~~~~~~~~~~~");
        target.writeMessage();
        System.out.println("\nProxy ~~~~~~~~~~~~~~~~~~~~");
        targetProxy.writeMessage();
    }

}
