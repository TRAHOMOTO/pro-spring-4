package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {

    public static void main(String[] args) {

        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();

        AppProperties props = ctx.getBean("appProperty", AppProperties.class);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("application.home: " +props.getApplicationHome());
        System.out.println("user.home: " +props.getUserHome());
    }

}
