package org.trahomoto.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trahomoto.MessageProvider;

@Service("messageProvider")
public class ConfigurableMessageProvider implements MessageProvider {

    private String message;

    // Правильный подход
    @Autowired
    public ConfigurableMessageProvider(String message) {
        this.message = message;
    }

//    Галимый практис
//    @Autowired
//    public ConfigurableMessageProvider(@Value("Сука верни Донбасс!") String message) {
//        this.message = message;
//    }

    @Override
    public String getMessage() {
        return message;
    }
}
