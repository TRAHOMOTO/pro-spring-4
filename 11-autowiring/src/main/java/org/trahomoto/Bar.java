package org.trahomoto;

public class Bar {
    public String getBarName() {
        return barName;
    }

    public void setBarName(String barName) {
        this.barName = barName;
    }

    private String barName;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Bar{");
        sb.append("barName='").append(barName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
