package org.trahomoto;

public class Foo {
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Foo{");
        sb.append("fooName='").append(fooName).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getFooName() {
        return fooName;
    }

    public void setFooName(String fooName) {
        this.fooName = fooName;
    }

    private String fooName;
}
