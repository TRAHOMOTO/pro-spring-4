package org.trahomoto;

import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();

        Target t = null;

        System.out.println("Using byName:");
        t = ctx.getBean("targetByName", Target.class);
        System.out.println(t);

        System.out.println("\nUsing byType:");
        t = ctx.getBean("targetByType", Target.class);
        System.out.println(t);

        System.out.println("\nUsing by constructor:");
        t = ctx.getBean("targetConstructor", Target.class);
        System.out.println(t);
    }
}
