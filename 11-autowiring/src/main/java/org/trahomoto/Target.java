package org.trahomoto;

public class Target {
    private Foo fooProp;
    private Bar bar;
    private Foo anotherFoo;


    public Target() {
    }

    public Target(Foo fooProp) {
        System.out.println("Target(Foo) called");
        this.fooProp = fooProp;
    }

    public Target(Foo fooProp, Bar bar) {

        System.out.println("Target(Foo, Bar) called");
        this.fooProp = fooProp;
        this.bar = bar;
    }

    public Foo getFooProp() {
        return fooProp;
    }

    public void setFooProp(Foo fooProp) {
        System.out.println("Property fooProp set");
        this.fooProp = fooProp;
    }

    public Bar getBar() {
        return bar;
    }

    public void setBar(Bar bar) {
        System.out.println("Property bar set");
        this.bar = bar;
    }

    public Foo getAnotherFoo() {
        return anotherFoo;
    }

    public void setAnotherFoo(Foo anotherFoo) {
        System.out.println("Property anotherFoo set");
        this.anotherFoo = anotherFoo;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Target{");
        sb.append("fooProp=").append(fooProp);
        sb.append(", bar=").append(bar);
        sb.append(", anotherFoo=").append(anotherFoo);
        sb.append('}');
        return sb.toString();
    }
}
