package org.trahomoto;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by komunar on 21.03.2016.
 */
public class MessageDigestFactory {

    String algorithmName = "MD5";
    private MessageDigest digest;


    public MessageDigest createInstance() throws NoSuchAlgorithmException {
        return MessageDigest.getInstance(algorithmName);
    }


    public void setAlgorithmName(String name){
        algorithmName = name;
    }

}
