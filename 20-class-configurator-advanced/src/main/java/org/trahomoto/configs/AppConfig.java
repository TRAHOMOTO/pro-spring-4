package org.trahomoto.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.trahomoto.MessageProvider;
import org.trahomoto.MessageRenderer;
import org.trahomoto.impl.ConfigurableMsgProvider;
import org.trahomoto.impl.ConsoleMessageRenderer;

/**
 • Аннотация @PropertySource применяется для загрузки файлов свойств в
 *  ApplicationContext и принимает в качестве арrумента местоположение (до-
 *  пускается указывать более одного местоположения). В ХМL той же самой цели
 *  служит дескриптор <context: property-placeholder>.
 *
 • Можно также использовать аннотацию @ImportResource для импортирова­
 *  ния конфигурации из ХМL-файлов, что означает возможность совместного
 *  применения ХМL и Jаvа-классов конфигурации, хотя поступать подобным
 *  образом не рекомендуется. Смешивание конфигураций XML и Java усложнит
 *  сопровождение приложения, поскольку в поисках специфического бина при­
 *  дется просматривать и ХМL-файлы, и Jаvа-классы.
 *
 • Кроме @ImportResource для импортирования других классов конфигурации
 *  можно использовать аннотацию @Import, а это значит, что можно иметь мно­
 *  жество Jаvа-классов конфигурации для разных конфигураций (например, один
 *  класс может быть выделен для объявления бинов ОАО, один - для объявле­
 *  ния бинов служб и т.д.).
 *
 • Аннотация @Component Scan определяет пакеты, которые платформа
 *  Spriпg должна сканировать на предмет аннотаций для определений бинов.
 *  Аналогичную роль играет дескриптор <context: component-scan> в ХМL­
 *  конфиrурации.
 *
 • Остальные аннотации самоочевидны. Аннотация @Lazy заставляет Spring
 *  создавать экземпляры бина, только когда он запрашивается (то же, что и
 *  lazy-init="true" в XML), а @DependsOn сообщает Sprig о том, что опре­
 *  деленный бин зависит от других бинов, поэтому Spring должна обеспечить со­
 *  здание их экземпляров первыми. Аннотация @Scope определяет область дейс­
 *  твия бина.
 *
 • Службы инфраструктуры приложения также могут быть определены в Jаvа­
 *  классах. Например, @EnaЫeTransactionManagement указывает, что будет
 *  применяться средство управления транзакциями Spring, которое подробно
 *  рассматривается в главе 9.
 *
 • Вы могли также заметить аннотацию @Autowired у переменной env типа
 *  Environment. Это средство абстрагирования Environment, предоставляемое
 *  Spriпg. Мы обсудим его далее в этой главе.
 */


@Configuration
@ImportResource(value = "classpath:app-context.xml")
@PropertySource(value = "classpath:message.properties")
@ComponentScan(basePackages = {"org.trahomoto"})
@EnableTransactionManagement
public class AppConfig {

    @Autowired
    Environment env;

    @Bean
    @Lazy(value = true)
    public MessageProvider messageProvider(){
        return new ConfigurableMsgProvider(env.getProperty("message"));
    }

    @Bean(name = "myMessageRenderer")
    @Scope(value = "prototype")
    @DependsOn(value = "messageProvider")
    public MessageRenderer messageRenderer(){
        MessageRenderer mr = new ConsoleMessageRenderer();
        mr.setMessageProvider(messageProvider());

        return mr;
    }

}
