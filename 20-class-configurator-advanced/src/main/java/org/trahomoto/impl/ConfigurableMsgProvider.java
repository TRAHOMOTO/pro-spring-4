package org.trahomoto.impl;

import org.trahomoto.MessageProvider;

public class ConfigurableMsgProvider implements MessageProvider {

    private String message = "Default message";

    public ConfigurableMsgProvider() {}

    public ConfigurableMsgProvider(String message) {
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
