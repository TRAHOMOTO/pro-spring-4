package org.trahomoto.performances;

public interface Poem {
    void recite();
}
