package org.trahomoto.members;


import org.trahomoto.performances.Poem;

public class PoeticJuggler extends Juggler{
    private Poem poem;

    public PoeticJuggler(Poem poem) {
        super();
        this.poem = poem;
    }

    public PoeticJuggler(int beanBags, Poem poem) {
        super(beanBags);
        this.poem = poem;
    }


    @Override
    public void perform() throws Exception {
        super.perform();
        System.out.println("И читает поэму...");
        poem.recite();
    }
}
