package org.trahomoto.members;


public interface Performer {
    public void perform() throws Exception;
}
