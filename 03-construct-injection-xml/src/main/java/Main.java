import org.springframework.context.support.GenericXmlApplicationContext;
import org.trahomoto.MessageProvider;

public class Main {

    public static void main(String[] args) {

        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:app-context-xml.xml");
        ctx.refresh();



        MessageProvider provider = ctx.getBean("messageProvider", MessageProvider.class);

        System.out.println("\n------------------------------------------------\n");

        System.out.println(provider.getMessage());
    }

}
